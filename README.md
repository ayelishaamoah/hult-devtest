# Career Mapper

This is a shareable report, the final page of an application. The user has built their career path, by selecting roles he desires to follow, and the skills he aready pocess for each role.

We're displaying the missing skills, for each role, and directing the user to appropiate programs.

# Approach

For this project I decided to use React, this is a technology that I am keen to gain more experience with and I thought that through using it for this project I would gain more insight on what it could be like working as part of the Hult development team as I know this is part of the tech stack.

## Project Instructions

1. Clone the repo and change into the project directory
2. run ``npm install` to install the required packages

### Running the project

run `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Running the tests

run `npm test`
