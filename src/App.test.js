import React from 'react';
import App from './App';
import { render } from '@testing-library/react';

it('renders the app component', () => {
  const app = render(<App />);
  expect(app).toMatchSnapshot();
});
