import React, { useState, useEffect } from 'react';
import './App.css';
import StudentSummaryCard from './components/StudentSummaryCard/StudentSummaryCard';
import ChosenRoleCard from './components/ChosenRoleCard/ChosenRoleCard';
import LongTermRoleCard from './components/LongTermRoleCard/LongTermRoleCard';
import SharingCard from './components/SharingCard/SharingCard';

function App() {
  const [reportDetails, setReportDetails] = useState(null);

  useEffect(() => {
    fetch('../report.json')
      .then(res => res.json())
      .then(res => setReportDetails(res))
      .catch(err => console.log(err));
  });
  if (reportDetails != null) {
    const studentData = {
      firstName: reportDetails.firstName,
      lastName: reportDetails.lastName,
      education: reportDetails.education,
      experience: reportDetails.experience,
      currentRole: reportDetails.currentRole,
      currentSalary: reportDetails.currentRoleDetails.salaryMean
    };

    const shortTermGoal = reportDetails.careerPath[0];
    const longTermGoal = reportDetails.careerPath[1];
    const longTermGoalSpecialized =
      longTermGoal.details.allSkills.specializedSkills;
    const longTermGoalExtraSkills =
      longTermGoal.details.allSkills.definingSkills;

    const shortTermRoleData = {
      chosenJobTitle: shortTermGoal.name,
      roleDemand: shortTermGoal.details.demandLevel,
      roleExperienceYears: shortTermGoal.details.experienceYears,
      roleMeanSalary: shortTermGoal.details.meanSalary,
      studentSalary: reportDetails.currentRoleDetails.salaryMean,
      topSkills: shortTermGoal.details.allSkills.definingSkills,
      extraSkills: shortTermGoal.details.allSkills.distinguishingSkills
    };
    const longTermRoleData = {
      chosenJobTitle: longTermGoal.name,
      roleExperienceYears: longTermGoal.details.experienceYears,
      roleMeanSalary: longTermGoal.details.salaryMean,
      studentSalary: reportDetails.currentRoleDetails.salaryMean,
      skills: longTermGoalSpecialized.concat(longTermGoalExtraSkills),
      allSkills: longTermGoal.details.allSkills
    };

    const programs = reportDetails.programs;
    return (
      <div className="App">
        <StudentSummaryCard {...studentData} />
        <ChosenRoleCard roleData={shortTermRoleData} />
        <LongTermRoleCard programs={programs} roleData={longTermRoleData} />
        <SharingCard />
      </div>
    );
  } else {
    return (
      <div className="App">
        <h1>Loading</h1>
      </div>
    );
  }
}

export default App;
