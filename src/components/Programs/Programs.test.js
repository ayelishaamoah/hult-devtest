import React from 'react';
import { cleanup, render } from '@testing-library/react';
import 'jest-dom/extend-expect';
import Programs from './Programs';

describe('Programs', () => {
  afterEach(cleanup);
  const programs = [
    {
      Title: 'Master of Computer Science',
      CoveredSkills: [
        'Budgeting',
        'Business Process',
        'Communication Skills',
        'Information Security',
        'Information Systems',
        'ITIL',
        'Policy Development',
        'Staff Management',
        'Stakeholder Management',
        'Teamwork / Collaboration'
      ],
      Link: 'https://online.stanford.edu/programs/computer-science-ms-degree',
      Description:
        'A master’s degree in Computer Science is intended as a terminal professional degree. Earning this degree indicates two things to prospective employers. First, it guarantees that you have a broad grounding in computer science as a discipline. Second, it certifies that you have studied a particular area in detail and thus have additional depth in a particular specialty. '
    },
    {
      Title: 'Master of Business Administration - Fall 2018 Intake',
      CoveredSkills: [
        'Financial Management',
        'Communication Skills',
        'Budgeting',
        'Building Effective Relationships',
        'Business Process',
        'Liquidity Risk',
        'Staff Management',
        'Stakeholder Management',
        'Strategic Planning',
        'Teamwork / Collaboration'
      ],
      Link: 'http://www.hult.edu/en/mba/one-year-mba/',
      Description:
        "Master essential business concepts in a global context, learn to lead international teams, and customize your to suit your personal goals with Hult's award-winning MBA. Addressing key areas including marketing, finance, operations, strategy, analytics, and leadership this degree is designed so that you'll continuously apply business knowledge as you acquire it and future-proof your career.\r\n\r\n40% of S&P 500 CEOs have an MBA."
    }
  ];

  const skillsNeeded = [
    'Communication Skills',
    'Planning',
    'Teamwork / Collaboration',
    'Problem Solving',
    'Building Effective Relationships',
    'Budgeting',
    'Project Management',
    'Information Systems',
    'Strategic Planning'
  ];

  it('renders the available programs', () => {
    const { container } = render(
      <Programs skillsNeeded={skillsNeeded} programs={programs} />
    );
    expect(container.firstChild.childNodes.length).toBe(2);
  });
});
