import React from 'react';
import './Programs.css';
import ProgramCard from '../ProgramCard/ProgramCard';

function Programs({ programs, skillsNeeded }) {
  const progamCards = programs.map((program, i) => {
    const programData = {
      title: program.Title,
      description: program.Description,
      coveredSkills: program.CoveredSkills,
      link: program.Link,
      availablePrograms: programs.length,
      programNumber: i + 1
    };

    return <ProgramCard skillsNeeded={skillsNeeded} key={i} {...programData} />;
  });
  return <div className="programs">{progamCards}</div>;
}

export default Programs;
