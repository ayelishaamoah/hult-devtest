import React from 'react';
import { getByText, render } from '@testing-library/react';
import 'jest-dom/extend-expect';
import { cleanup } from '@testing-library/react';
import SkillTile from './SkillTile';

describe('Skill Tile', () => {
  afterEach(cleanup);

  const skillName = 'Test Driven Development (TDD)';

  it('displays the name of a skills', () => {
    const { container } = render(<SkillTile skillName={skillName} />);
    expect(getByText(container, skillName)).toBeDefined();
  });
});
