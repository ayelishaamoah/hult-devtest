import React from 'react';
import './SkillTile.css';

function SkillTile({ skillName }) {
  return (
    <div className="skill-tile">
      <p>{skillName}</p>
    </div>
  );
}

export default SkillTile;
