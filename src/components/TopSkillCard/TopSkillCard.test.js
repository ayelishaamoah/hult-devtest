import React from 'react';
import { cleanup, render } from '@testing-library/react';
import TopSkillCard from './TopSkillCard';
import 'jest-dom/extend-expect';

describe('Top Skill Card', () => {
  const skill = {
    name: 'Software Engineering',
    description:
      'Working experience with the application of engineering to the development of software in a systematic method.',
    count: 44937
  };

  afterEach(cleanup);
  it('displays the name of the skill', () => {
    const { getByText, container } = render(
      <TopSkillCard name={skill.name} description={skill.description} />
    );
    expect(getByText('Software Engineering', container)).toBeDefined();
  });

  it('displays the description of the skill', () => {
    const { getByText, container } = render(
      <TopSkillCard name={skill.name} description={skill.description} />
    );
    expect(getByText(`${skill.description}`, container)).toBeDefined();
  });
});
