import React, { useState } from 'react';
import './TopSkillCard.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFire, faChevronDown } from '@fortawesome/free-solid-svg-icons';

function TopSkillCard({ name, description }) {
  const [detailVisible, setDetailState] = useState(false);
  return (
    <div className="skill__tile">
      <div className="skill__main">
        <div className="skill__content">
          <FontAwesomeIcon className="skill__icon" icon={faFire} />
          <p className="skill__title">{name}</p>
        </div>
        <div>
          <button
            className="skills__toggle-button"
            onClick={() =>
              setDetailState(prevDetailVisible => !prevDetailVisible)
            }
          >
            <FontAwesomeIcon
              className="skill__toggle-icon"
              icon={faChevronDown}
            />
          </button>
        </div>
      </div>
      <p
        className={`skill__description ${
          detailVisible
            ? 'skill__description-visible'
            : 'skill__description-hidden'
        }`}
      >
        {description}
      </p>
    </div>
  );
}
export default TopSkillCard;
