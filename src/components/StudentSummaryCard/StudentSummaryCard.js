import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUser,
  faBriefcase,
  faGraduationCap,
  faClock
} from '@fortawesome/free-solid-svg-icons';
import './StudentSummaryCard.css';

function StudentSummaryCard({
  firstName,
  lastName,
  currentRole,
  experience,
  education
}) {
  const name = `${firstName} ${lastName}`;
  return (
    <section className="student-summary-card__wrapper" role="banner">
      <h1 className="summary-card__title">{name}'s career path</h1>
      <div className="student-summary-card ">
        <div className="summary__header">
          <FontAwesomeIcon
            id="profile__icon"
            className="summary__icon"
            icon={faUser}
          />
          <p className="summary__title" data-testid="name">
            {name}
          </p>
        </div>
        <div className="summary__list" data-testid="student-details">
          <div className="summary__row">
            <FontAwesomeIcon className="summary__icon" icon={faBriefcase} />
            <p className="summary__item" data-testid="current-role">
              {currentRole}
            </p>
          </div>
          <div className="summary__row">
            <FontAwesomeIcon className="summary__icon" icon={faGraduationCap} />
            <p className="summary__item" data-testid="education">
              {education}
            </p>
          </div>
          <div className="summary__row">
            <FontAwesomeIcon className="summary__icon" icon={faClock} />
            <p className="summary__item" data-testid="experience">
              {experience} years
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default StudentSummaryCard;
