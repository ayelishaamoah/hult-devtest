import React from 'react';

import { render, cleanup, getByTestId } from '@testing-library/react';
import 'jest-dom/extend-expect';
import StudentSummaryCard from './StudentSummaryCard';

describe('Student Summary Card', () => {
  afterEach(cleanup);

  const props = {
    firstName: 'John',
    lastName: 'Doe',
    education: "Bachelor's Degree",
    experience: 'Less than 3',
    currentRole: 'Software Developer / Engineer'
  };

  it('renders the student summary card component', () => {
    const component = render(<StudentSummaryCard {...props} />);
    expect(component).toMatchSnapshot();
  });

  it(' displays the student name', () => {
    const { container } = render(<StudentSummaryCard {...props} />);
    expect(getByTestId(container, 'name')).toHaveTextContent('John Doe');
  });

  it(' displays the student summary', () => {
    const { container } = render(<StudentSummaryCard {...props} />);
    expect(getByTestId(container, 'current-role')).toHaveTextContent(
      'Software Developer / Engineer'
    );
    expect(getByTestId(container, 'experience')).toHaveTextContent(
      'Less than 3'
    );
    expect(getByTestId(container, 'education')).toHaveTextContent(
      "Bachelor's Degree"
    );
  });
});
