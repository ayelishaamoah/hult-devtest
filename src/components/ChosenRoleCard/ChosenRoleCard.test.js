import React from 'react';

import { render, cleanup, getByTestId } from '@testing-library/react';
import 'jest-dom/extend-expect';
import ChosenRoleCard from './ChosenRoleCard';

describe('Chosen Role Card', () => {
  afterEach(cleanup);
  const skills = [
    {
      name: 'Software Engineering',
      description:
        'Working experience with the application of engineering to the development of software in a systematic method.',
      count: 44937
    },
    {
      name: 'Software Development',
      description:
        'Software development is the process of computer programming, documenting, testing, and bug fixing involved in creating and maintaining applications and frameworks resulting in a software product.',
      count: 33988
    },
    {
      name: 'Java',
      description:
        'Java is a set of computer software and specifications developed by Sun Microsystems, which was later acquired by the Oracle Corporation, that provides a system for developing application software and deploying it in a cross-platform computing environment.',
      count: 26077
    }
  ];

  const roleData = {
    chosenJobTitle: 'Senior Software Developer / Engineer',
    roleDemand: 'High',
    roleExperienceYears: 5,
    roleMeanSalary: 109374.97,
    studentSalary: 95058.16,
    topSkills: skills,
    extraSkills: skills
  };

  it('renders all children components', () => {
    const { container } = render(<ChosenRoleCard roleData={roleData} />);
    const roleCard = getByTestId(container, 'chosen-role-card');
    expect(roleCard.children.length).toBe(3);
  });
});
