import React from 'react';
import './ChosenRoleCard.css';
import ChosenRoleSummary from '../ChosenRoleSummary/ChosenRoleSummary';
import TopSkills from '../TopSkills/TopSkills';
import SkillsCard from '../SkillsCard/SkillsCard';

function ChosenRoleCard({ roleData }) {
  const topSkillsData = roleData.topSkills;

  const extraSkills = roleData.extraSkills.map(skill => {
    return skill.name;
  });

  const skillsHeader = 'Other skills typically needed for this role';
  return (
    <section className="chosen-role-card__wrapper">
      <h2 className="chosen-role-card__title">Your short-term goal</h2>
      <div data-testid="chosen-role-card" className="chosen-role-card">
        <ChosenRoleSummary {...roleData} />
        <h4 className="chosen-role-card__subtitle">
          Top 3 most in-demand skills for this role
        </h4>
        <TopSkills skillsData={topSkillsData} />
        <SkillsCard header={skillsHeader} skills={extraSkills} />
      </div>
    </section>
  );
}

export default ChosenRoleCard;
