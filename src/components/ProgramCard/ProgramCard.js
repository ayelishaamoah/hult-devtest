import React from 'react';
import './ProgramCard.css';

function ProgramCard({
  title,
  description,
  coveredSkills,
  skillsNeeded,
  link,
  programNumber,
  availablePrograms
}) {
  const programSkills = new Set(
    coveredSkills.map(item => {
      return item.Title;
    })
  );
  let matchCount = 0;

  skillsNeeded.forEach(skill => {
    if (programSkills.has(skill)) {
      matchCount += 1;
    }
    return matchCount;
  });

  const percentageSkills = ((matchCount / skillsNeeded.size) * 100).toFixed(0);

  return (
    <section className="program-card">
      <p className="program-card__option">
        Option {programNumber} of {availablePrograms}
      </p>
      <h4 className="program-card__title">{title}</h4>
      <p className="program-card__description">{description}</p>
      <p className="program-card__skill-ratio">
        {percentageSkills}% of the required skills covered
      </p>
      <button className="program-card__button">
        <a className="program-card__link" href={link}>
          Go to Program
        </a>
      </button>
    </section>
  );
}

export default ProgramCard;
