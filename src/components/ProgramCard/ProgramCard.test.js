import React from 'react';
import 'jest-dom/extend-expect';
import { render, cleanup, getByText } from '@testing-library/react';
import ProgramCard from './ProgramCard';

describe('Program Card', () => {
  afterEach(cleanup);
  const program = {
    title: 'Master of Computer Science',
    coveredSkills: [
      { Title: 'Budgeting', Id: 'a3w1Q0000002OydQAE' },
      { Title: 'Business Process', Id: 'a3w1Q0000002OyjQAE' },
      { Title: 'Communication Skills', Id: 'a3w36000000kHqlAAE' },
      { Title: 'Information Security', Id: 'a3w36000000kHhYAAU' },
      { Title: 'Information Systems', Id: 'a3w1Q0000002P9vQAE' },
      { Title: 'ITIL', Id: 'a3w1Q0000002PA5QAM' },
      { Title: 'Policy Development', Id: 'a3w1Q0000002PAAQA2' },
      { Title: 'Staff Management', Id: 'a3w1Q0000002PAKQA2' },
      { Title: 'Stakeholder Management', Id: 'a3w1Q0000002PABQA2' },
      { Title: 'Teamwork / Collaboration', Id: 'a3w1Q0000002OybQAE' }
    ],
    link: 'https://online.stanford.edu/programs/computer-science-ms-degree',
    description:
      'A master’s degree in Computer Science is intended as a terminal professional degree. Earning this degree indicates two things to prospective employers. First, it guarantees that you have a broad grounding in computer science as a discipline. Second, it certifies that you have studied a particular area in detail and thus have additional depth in a particular specialty.',
    availablePrograms: 1,
    programNumber: 1
  };

  const skillsNeeded = new Set([
    'Communication Skills',
    'Planning',
    'Teamwork / Collaboration',
    'Problem Solving',
    'Building Effective Relationships',
    'Budgeting',
    'Project Management',
    'Information Systems',
    'Strategic Planning',
    'Business Process',
    'ITIL'
  ]);

  it('displays the program title', () => {
    const { container } = render(
      <ProgramCard skillsNeeded={skillsNeeded} {...program} />
    );
    expect(getByText(container, 'Master of Computer Science')).toBeDefined();
  });

  it('displays the option number', () => {
    const { container } = render(
      <ProgramCard skillsNeeded={skillsNeeded} {...program} />
    );
    expect(getByText(container, 'Option 1 of 1')).toBeDefined();
  });

  it('displays the program description', () => {
    const { container } = render(
      <ProgramCard skillsNeeded={skillsNeeded} {...program} />
    );
    expect(getByText(container, program.description)).toBeDefined();
  });

  it('displays the program link', () => {
    const { container } = render(
      <ProgramCard skillsNeeded={skillsNeeded} {...program} />
    );
    const programLink = container.querySelector('button').firstChild;
    expect(programLink.href).toBe(program.link);
  });

  it('displays the percentage of skills covered link', () => {
    const { container } = render(
      <ProgramCard skillsNeeded={skillsNeeded} {...program} />
    );
    expect(
      getByText(container, '55% of the required skills covered')
    ).toBeDefined();
  });
});
