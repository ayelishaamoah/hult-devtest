import React from 'react';
import { render, getByText } from '@testing-library/react';
import 'jest-dom/extend-expect';
import SkillsCard from './SkillsCard';
import { cleanup } from '@testing-library/react';

describe('Skills Card', () => {
  afterEach(cleanup);

  const skills = [
    'Test Driven Development (TDD)',
    'PERL Scripting Language',
    'Visual Studio',
    'PostgreSQL',
    'Ruby'
  ];
  it('renders skills tiles', () => {
    const { container } = render(<SkillsCard skills={skills} />);
    expect(getByText(container, 'Test Driven Development (TDD)')).toBeDefined();
  });
});
