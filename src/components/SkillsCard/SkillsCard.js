import React from 'react';
import SkillTile from '../SkillTile/SkillTile';
import './SkillsCard.css';

function SkillsCard({ skills, header }) {
  const skillsTiles = skills.map((skill, i) => {
    return <SkillTile skillName={skill} key={i} />;
  });
  return (
    <div className="skills-card__wrapper">
      <h4 className="skills-card__title">{header}</h4>
      <div className="skills-card">{skillsTiles}</div>
    </div>
  );
}

export default SkillsCard;
