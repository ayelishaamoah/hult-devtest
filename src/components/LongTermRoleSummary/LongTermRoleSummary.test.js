import React from 'react';
import { render, cleanup, getByText } from '@testing-library/react';
import 'jest-dom/extend-expect';
import LongTermRoleSummary from './LongTermRoleSummary';

describe('Long Term Role', () => {
  afterEach(cleanup);

  const props = {
    chosenJobTitle: 'Chief Information Officer',
    roleExperienceYears: 6,
    roleMeanSalary: 128159.81,
    studentSalary: 95058.16
  };

  it('displays the chosen role job title', () => {
    const { container } = render(<LongTermRoleSummary {...props} />);
    expect(getByText(container, 'Chief Information Officer')).toBeDefined();
  });

  it('displays the chosen roles required experience', () => {
    const { container } = render(<LongTermRoleSummary {...props} />);
    expect(getByText(container, '6 years experience')).toBeDefined();
  });

  it('displays the chosen roles salary increase', () => {
    const { container } = render(<LongTermRoleSummary {...props} />);
    expect(getByText(container, '+35%')).toBeDefined();
  });
});
