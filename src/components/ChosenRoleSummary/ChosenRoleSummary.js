import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFlag,
  faDollarSign,
  faClock,
  faChartLine
} from '@fortawesome/free-solid-svg-icons';
import './ChosenRoleSummary.css';

function ChosenRoleSummary({
  chosenJobTitle,
  roleDemand,
  roleExperienceYears,
  roleMeanSalary,
  studentSalary
}) {
  const salaryChange = (roleMeanSalary - studentSalary) / studentSalary;
  const percentChange = (salaryChange * 100).toFixed(0);
  return (
    <section className="chosen-role-summary">
      <div className="chosen-role__header">
        <FontAwesomeIcon
          id="chosen-role__icon"
          className="summary__icon"
          icon={faFlag}
        />
        <h3 className="chosen-role__title" data-testid="job-title">
          {chosenJobTitle}
        </h3>
      </div>
      <div className="chosen-role__summary">
        <div className="summary__row">
          <FontAwesomeIcon
            className="summary__icon positive-change"
            icon={faDollarSign}
          />
          <p
            className="summary__item positive-change"
            data-testid="salary-change"
          >
            +{percentChange}%
          </p>
        </div>
        <div className="summary__row">
          <FontAwesomeIcon
            className={`summary__icon ${roleDemand.toLowerCase()}-demand`}
            icon={faChartLine}
          />
          <p
            className={`summary__item ${roleDemand.toLowerCase()}-demand`}
            data-testid="job-demand"
          >
            {roleDemand} market demand
          </p>
        </div>
        <div className="summary__row">
          <FontAwesomeIcon className="summary__icon" icon={faClock} />
          <p className="summary__item" data-testid="years-experience">
            {roleExperienceYears} years experience
          </p>
        </div>
      </div>
    </section>
  );
}

export default ChosenRoleSummary;
