import React from 'react';
import { render, cleanup, getByTestId } from '@testing-library/react';
import 'jest-dom/extend-expect';
import ChosenRoleSummary from './ChosenRoleSummary';

describe('Chosen Role', () => {
  afterEach(cleanup);

  const props = {
    chosenJobTitle: 'Senior Software Developer / Engineer',
    roleDemand: 'High',
    roleExperienceYears: 5,
    roleMeanSalary: 109374.97,
    studentSalary: 95058.16
  };

  it('displays the chosen role job title', () => {
    const { container } = render(<ChosenRoleSummary {...props} />);
    expect(getByTestId(container, 'job-title')).toHaveTextContent(
      'Senior Software Developer / Engineer'
    );
  });

  it('displays the chosen roles demand', () => {
    const { container } = render(<ChosenRoleSummary {...props} />);
    expect(getByTestId(container, 'job-demand')).toHaveTextContent(
      'High market demand'
    );
  });

  it('displays the chosen roles required experience', () => {
    const { container } = render(<ChosenRoleSummary {...props} />);
    expect(getByTestId(container, 'years-experience')).toHaveTextContent(
      '5 years experience'
    );
  });

  it('displays the chosen roles salary increase', () => {
    const { container } = render(<ChosenRoleSummary {...props} />);
    expect(getByTestId(container, 'salary-change')).toHaveTextContent('+15%');
  });
});
