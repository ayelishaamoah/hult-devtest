import React from 'react';
import { cleanup, render } from '@testing-library/react';
import TopSkills from './TopSkills';
import 'jest-dom/extend-expect';

describe('Top Skills', () => {
  const skills = [
    {
      name: 'Software Engineering',
      description:
        'Working experience with the application of engineering to the development of software in a systematic method.',
      count: 44937
    },
    {
      name: 'Software Development',
      description:
        'Software development is the process of computer programming, documenting, testing, and bug fixing involved in creating and maintaining applications and frameworks resulting in a software product.',
      count: 33988
    },
    {
      name: 'Java',
      description:
        'Java is a set of computer software and specifications developed by Sun Microsystems, which was later acquired by the Oracle Corporation, that provides a system for developing application software and deploying it in a cross-platform computing environment.',
      count: 26077
    },
    {
      name: 'SQL',
      description:
        'SQL ( ESS-kew-EL or SEE-kwl, Structured Query Language) is a domain-specific language used in programming and designed for managing data held in a relational database management system (RDBMS), or for stream processing in a relational data stream management system (RDSMS).',
      count: 18368
    },
    {
      name: 'JavaScript',
      description:
        'JavaScript (), often abbreviated as JS, is a high-level, dynamic, weakly typed, object-based, multi-paradigm, and interpreted client-side programming language.',
      count: 17672
    }
  ];

  afterEach(cleanup);
  it('renders the top three skills', () => {
    const { container } = render(<TopSkills skillsData={skills} />);
    expect(container.firstChild.children.length).toBe(3);
  });
});
