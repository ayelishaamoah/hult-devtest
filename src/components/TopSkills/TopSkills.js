import React from 'react';
import './TopSkills.css';
import TopSkillCard from '../TopSkillCard/TopSkillCard';

function TopSkills({ skillsData }) {
  const topThreeSkills = skillsData.slice(0, 3);
  const topSkills = topThreeSkills.map((skill, i) => {
    return (
      <TopSkillCard name={skill.name} description={skill.description} key={i} />
    );
  });

  return <div className="top-skills">{topSkills}</div>;
}
export default TopSkills;
