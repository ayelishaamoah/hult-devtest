import React from 'react';
import './LongTermRoleCard.css';
import LongTermRoleSummary from '../LongTermRoleSummary/LongTermRoleSummary';
import SkillsCard from '../SkillsCard/SkillsCard';
import Programs from '../Programs/Programs';

function LongTermRoleCard({ roleData, programs }) {
  const skills = roleData.skills.map(skill => {
    return skill.name;
  });
  const allSkills = [];
  for (const skillGroup in roleData.allSkills) {
    roleData.allSkills[skillGroup].forEach(skill => {
      allSkills.push(skill.name);
    });
  }

  const skillsHeader = 'Skills typically needed for this role';

  return (
    <section className="long-term-role-card__wrapper">
      <h2 className="long-term-role-card__title">Your long-term goal</h2>
      <div data-testid="long-term-role-card" className="long-term-role-card">
        <LongTermRoleSummary {...roleData} />
        <SkillsCard header={skillsHeader} skills={skills} />
        <Programs skillsNeeded={new Set(allSkills)} programs={programs} />
      </div>
    </section>
  );
}

export default LongTermRoleCard;
