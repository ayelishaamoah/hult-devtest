import React from 'react';
import { cleanup, render } from '@testing-library/react';
import 'jest-dom/extend-expect';
import SharingCard from './SharingCard';

describe('Sharing Card', () => {
  afterEach(cleanup);
  it('renders the sharing card', () => {
    const sharingCard = render(<SharingCard />);
    expect(sharingCard).toMatchSnapshot();
  });
});
