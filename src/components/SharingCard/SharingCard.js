import React from 'react';
import './SharingCard.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShare } from '@fortawesome/free-solid-svg-icons';

function SharingCard() {
  return (
    <aside className="sharing-card">
      <button className="sharing-card__button">
        <a className="sharing-card__link" href="mailto:" target="_top">
          Share your plan
        </a>
        <FontAwesomeIcon id="sharing-card__icon" icon={faShare} />
      </button>
      <p className="sharing-card__fineprint">
        <span className="text-highlight">Career Mapper</span> data is provided
        by various leading public & private big data sources
      </p>
    </aside>
  );
}
export default SharingCard;
